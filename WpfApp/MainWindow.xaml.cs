﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HttpClient client = new HttpClient();
        ProductsCollection _products = new ProductsCollection(); 
        public MainWindow()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://localhost:9000"); 
            client.DefaultRequestHeaders.Accept.Add( 
                new MediaTypeWithQualityHeaderValue("application/json")); 

            this.ProductsList.ItemsSource = _products; 
        }

        private async void GetProducts(object sender, RoutedEventArgs e)
        {
            try
            {
                btnGetProducts.IsEnabled = false;

                var response = await client.GetAsync("api/products");
                response.EnsureSuccessStatusCode(); // Throw on error code（有错误码时报出异常）.

                var products = await response.Content.ReadAsAsync<IEnumerable<Product>>();
                _products.CopyFrom(products);

            }
            catch (Newtonsoft.Json.JsonException jEx)
            {
                // This exception indicates a problem deserializing the request body.
                // 这个异常指明了一个解序列化请求体的问题。
                MessageBox.Show(jEx.Message);
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                btnGetProducts.IsEnabled = true;
            }
        }

        private async void PostProduct(object sender, RoutedEventArgs e)
        {
            btnPostProduct.IsEnabled = false;

            try
            {
                var product = new Product()
                {
                    Name = textName.Text,
                    Price = decimal.Parse(textPrice.Text),
                    Category = textCategory.Text
                };
                var response = await client.PostAsJsonAsync("api/products", product);
                response.EnsureSuccessStatusCode(); // Throw on error code（有错误码时抛出）. 

                _products.Add(product);
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (System.FormatException)
            {
                MessageBox.Show("Price must be a number");
            }
            finally
            {
                btnPostProduct.IsEnabled = true;
            } 
        }

    }
}

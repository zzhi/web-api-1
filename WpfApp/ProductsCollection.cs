﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp
{
    class ProductsCollection : ObservableCollection<Product>
    {
        public void CopyFrom(IEnumerable<Product> products)
        {
            this.Items.Clear();
            foreach (var p in products)
            {
                this.Items.Add(p);
            }

            this.OnCollectionChanged(
                new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}

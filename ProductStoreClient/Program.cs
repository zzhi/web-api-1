﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Threading;

namespace ProductStoreClient
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            RunAsync();
            Console.WriteLine("hi");

            Console.Read();
        }

        private static async void RunAsync()
        {
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:9000/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // New code:
                try
                {
                    HttpResponseMessage response = await client.GetAsync("api/products/2");//非阻塞
                    //HttpResponseMessage response = client.GetAsync("api/products/2").Result; //阻塞
                   
                    if (response.IsSuccessStatusCode)
                    {
                        Product product = await response.Content.ReadAsAsync<Product>();
                        //Product product = response.Content.ReadAsAsync<Product>().Result;
                        Console.WriteLine("{0}\t${1}\t{2}", product.Name, product.Price, product.Category);
                    }



                    // HTTP POST
                    var gizmo = new Product() { Name = "Gizmo", Price = 100, Category = "Widget" };
                    response = await client.PostAsJsonAsync("api/products", gizmo);
                    if (response.IsSuccessStatusCode)
                    {
                        Uri gizmoUrl = response.Headers.Location;

                        // HTTP PUT
                        gizmo.Price = 80;   // Update price
                        response = await client.PutAsJsonAsync(gizmoUrl, gizmo);

                        // HTTP DELETE
                        response = await client.DeleteAsync(gizmoUrl);
                    }
                }
                catch(HttpRequestException e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
    }
}